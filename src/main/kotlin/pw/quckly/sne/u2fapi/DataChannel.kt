package pw.quckly.sne.u2fapi

import java.io.Closeable
import javax.smartcardio.CommandAPDU
import javax.smartcardio.ResponseAPDU

/***
 * Can be used in future for example in SSH or OpenVPN.
 * That means, U2FApiProxy can provide authenticate/register API not only for directly connected U2F tokens
 * (like NFC or HID) but thought some data channel like SSH connection.
 */
interface DataChannel : Closeable {
    fun transmit(data: CommandAPDU): ResponseAPDU
}
