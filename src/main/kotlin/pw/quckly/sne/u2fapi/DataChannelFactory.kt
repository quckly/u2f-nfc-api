package pw.quckly.sne.u2fapi

import org.springframework.stereotype.Component

@Component
class DataChannelFactory {

    fun getDataChannel(timeout: Int): DataChannel {
        return NfcDataChannel(timeout)
    }

    fun getDataChannel(): DataChannel {
        return NfcDataChannel()
    }
}