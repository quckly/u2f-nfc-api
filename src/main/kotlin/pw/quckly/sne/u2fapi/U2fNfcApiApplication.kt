package pw.quckly.sne.u2fapi

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import javax.smartcardio.CommandAPDU
import javax.smartcardio.TerminalFactory

@SpringBootApplication
class U2fNfcApiApplication {

    @Bean
    fun commandLineRunner(ctx: ApplicationContext): CommandLineRunner {
        return CommandLineRunner { args ->

        }
    }
}

fun main(args: Array<String>) {
    runApplication<U2fNfcApiApplication>(*args)
}
