package pw.quckly.sne.u2fapi.api

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import pw.quckly.sne.u2fapi.u2f.*

data class ErrorResponse(val status: Int,
                         val message: String,
                         val errorCode: String)

@RestController
class U2FHttpApiController(val u2fProxy: U2FApiProxy) {

    @PostMapping("/register")
    fun register(@RequestBody request: ApiRegisterRequest): ApiRegisterResponse {
        return u2fProxy.register(request)
    }

    @PostMapping("/authenticate")
    fun login(@RequestBody request: ApiAuthenticateRequest): ApiAuthenticateResponse {
        return u2fProxy.authenticate(request)
    }
}

@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    fun handleExceptions(ex: Exception, request: WebRequest): ResponseEntity<ErrorResponse> {
        ex.printStackTrace()

        val response = ErrorResponse(400,
                ex.message ?: "",
                ex.javaClass.name)

        return ResponseEntity(response, HttpHeaders(), HttpStatus.BAD_REQUEST)
    }
}
