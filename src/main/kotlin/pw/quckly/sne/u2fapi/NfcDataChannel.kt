package pw.quckly.sne.u2fapi

import java.io.Closeable
import java.util.concurrent.TimeoutException
import javax.smartcardio.*

class NfcDataChannel(val timeout: Int = 1000) : DataChannel {

    val terminal: CardTerminal
    val card: Card
    val channel: CardChannel

    init {
        // Display the list of terminals
        val factory = TerminalFactory.getDefault()
        val terminals = factory.terminals().list()
        println("Terminals: $terminals")

        // Use the first terminal
        terminal = terminals[0]

        // Connect with the card
        if (!terminal.waitForCardPresent(timeout.toLong())) {
            throw TimeoutException()
        }

        card = terminal.connect("*")
        println("card: $card")
        channel = card.basicChannel
    }

    override fun transmit(data: CommandAPDU): ResponseAPDU {
        return channel.transmit(data)
    }

    override fun close() {
        // Disconnect the card
        card.disconnect(false)
    }
}
