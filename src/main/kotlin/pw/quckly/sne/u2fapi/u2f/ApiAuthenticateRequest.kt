package pw.quckly.sne.u2fapi.u2f

/***
 *
 * @param appId The application id that the RP would like to assert for this key handle, if it's distinct from the application id for the overall request.
 * @param challenge The websafe-base64-encoded challenge.
 * @param keyHandle The registered keyHandle to use for signing, as a websafe-base64 encoding of the key handle bytes returned by the U2F token during registration.
 * @param origin the facet id of the caller, i.e., the web origin of the relying party.
 */
data class ApiAuthenticateRequest(val appId: String,
                                  val challenge: String,
                                  val keyHandle: String,
                                  val origin: String)