package pw.quckly.sne.u2fapi.u2f

/***
 *
 * @param keyHandle The keyHandle of the RegisteredKey that was processed.
 * @param signatureData The raw response from U2F device, websafe-base64 encoded.
 * @param clientData The client data created by the FIDO client, websafe-base64 encoded.
 */
data class ApiAuthenticateResponse (val keyHandle: String,
                                    val signatureData: String,
                                    val clientData: String)