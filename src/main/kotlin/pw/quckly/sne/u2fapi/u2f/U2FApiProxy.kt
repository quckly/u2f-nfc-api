package pw.quckly.sne.u2fapi.u2f

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import pw.quckly.sne.u2fapi.DataChannel
import pw.quckly.sne.u2fapi.DataChannelFactory
import java.lang.Exception
import java.security.MessageDigest
import java.util.*
import javax.smartcardio.CommandAPDU

data class ApduCreatedMessage(val messageBytes: ByteArray,
                              val clientData: ByteArray)

@Component
class U2FApiProxy(private val dataChannelFactory: DataChannelFactory) {

    @Value("\${app.nfc.attempts}")
    var nfcAttempts: Int = 1

    @Value("\${app.nfc.attemptTimeout}")
    var nfcAttemptTimeout: Int = 5000

    val jsonMapper = jacksonObjectMapper()
    val digest = MessageDigest.getInstance("SHA-256")
    val base64Coder = Base64.getUrlEncoder()
    val base64Decoder = Base64.getUrlDecoder()

    fun register(request: ApiRegisterRequest): ApiRegisterResponse {
        synchronized(this) {
            dataChannelFactory.getDataChannel(nfcAttemptTimeout).use { channel ->
                //val resp1 = channel.transmit(CommandAPDU(0xFF, 0x00, 0x40, 76, byteArrayOf(0x01, 0x01, 0x07, 0x00),0x00)) // Buzzer
                val resp2 = channel.transmit(CommandAPDU(0xFF, 0x00, 0x41, 0x02, 256)) // Set Timeout in P2 = *5sec

                doU2FSelect(channel)

                val message = makeRegisterMessage(request)

                val messageResponse = channel.transmit(CommandAPDU(0x00, 0x01, 0x03, 0x00, message.messageBytes, 256))

                if (messageResponse.sw != 0x9000) {
                    throw Exception("Error in registration request. SW = ${messageResponse.sw}, Data = ${messageResponse.data}")
                }

                return ApiRegisterResponse(request.version,
                        base64Coder.encodeToString(messageResponse.data),
                        base64Coder.encodeToString(message.clientData))
            }
        }
    }

    fun authenticate(request: ApiAuthenticateRequest): ApiAuthenticateResponse {
        synchronized(this) {
            val channel = dataChannelFactory.getDataChannel(nfcAttemptTimeout)

            doU2FSelect(channel)

            val message = makeAuthenticateMessage(request)

            val messageResponse = channel.transmit(CommandAPDU(0x00, 0x02, 0x03, 0x00, message.messageBytes, 256))

            if (messageResponse.sw != 0x9000) {
                throw Exception("Error in authentication request. SW = ${messageResponse.sw}, Data = ${messageResponse.data}")
            }

            return ApiAuthenticateResponse(request.keyHandle,
                    base64Coder.encodeToString(messageResponse.data),
                    base64Coder.encodeToString(message.clientData))
        }
    }

    fun doU2FSelect(channel: DataChannel) {
        // Send Select Applet command
        val aid = byteArrayOf(0xA0.toByte(), 0x00, 0x00, 0x06, 0x47, 0x2F, 0x00, 0x01)
        val answer = channel.transmit(CommandAPDU(0x00, 0xA4, 0x04, 0x00, aid))

        println("answer: " + answer.toString())
        println("U2F Version: ${answer.bytes.toString(Charsets.UTF_8)}")

        val responseBytes = answer.bytes.clone()
        if (!responseBytes.contentEquals(byteArrayOf(0x55, 0x32, 0x46, 0x5F, 0x56, 0x32, 0x90.toByte(), 0x00))) {
            throw Exception("Wrong select response. Response is: ${responseBytes.joinToString()}")
        }
    }

    fun makeRegisterMessage(request: ApiRegisterRequest): ApduCreatedMessage {
        val clientData = ClientData("navigator.id.finishEnrollment",
                request.challenge,
                request.origin)

        val clientDataJson = jsonMapper.writeValueAsString(clientData)
        val clientDataBytes = clientDataJson.toByteArray(Charsets.UTF_8)
        val clientDataBytesDigest = digest.digest(clientDataBytes)

        val applicationBytes = digest.digest(request.appId.toByteArray(Charsets.UTF_8))

        val apduBytes = clientDataBytesDigest + applicationBytes

        return ApduCreatedMessage(apduBytes, clientDataBytes)
    }

    fun makeAuthenticateMessage(request: ApiAuthenticateRequest): ApduCreatedMessage {
        val clientData = ClientData("navigator.id.getAssertion",
                request.challenge,
                request.origin)

        val clientDataJson = jsonMapper.writeValueAsString(clientData)
        val clientDataBytes = clientDataJson.toByteArray(Charsets.UTF_8)
        val clientDataBytesDigest = digest.digest(clientDataBytes)

        val applicationBytes = digest.digest(request.appId.toByteArray(Charsets.UTF_8))

        val keyHandleBytes = base64Decoder.decode(request.keyHandle)

        val apduBytes = clientDataBytesDigest +
                applicationBytes +
                byteArrayOf(keyHandleBytes.size.toByte()) +
                keyHandleBytes

        return ApduCreatedMessage(apduBytes, clientDataBytes)
    }
}