package pw.quckly.sne.u2fapi.u2f

/***
 *
 * @param appId The application id that the RP would like to assert for this key handle, if it's distinct from the application id for the overall request.
 * @param version The version of the protocol that the to-be-registered token must speak. E.g. "U2F_V2".
 * @param challenge The websafe-base64-encoded challenge.
 * @param origin the facet id of the caller, i.e., the web origin of the relying party.
 */
data class ApiRegisterRequest(val appId: String,
                              val version: String,
                              val challenge: String,
                              val origin: String)