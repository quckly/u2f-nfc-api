package pw.quckly.sne.u2fapi.u2f

/**
 *
 * @param typ the constant 'navigator.id.getAssertion' for authentication, and 'navigator.id.finishEnrollment' for registration
 * @param challenge the websafe-base64-encoded challenge provided by the relying party
 * @param origin the facet id of the caller, i.e., the web origin of the relying party.
 */
data class ClientData(val typ: String,
                      val challenge: String,
                      val origin: String)
