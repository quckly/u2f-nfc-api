package pw.quckly.sne.u2fapi.u2f

/***
 *
 * @param version The version of the protocol that the registered token speaks. E.g. "U2F_V2".
 * @param registrationData The raw registration response websafe-base64
 * @param clientData The client data created by the FIDO client, websafe-base64 encoded.
 */
data class ApiRegisterResponse (val version: String,
                                val registrationData: String,
                                val clientData: String)