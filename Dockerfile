FROM gradle:4.10.2-jdk8 as builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -Dorg.gradle.daemon=true

FROM openjdk:8u181-jre-slim-stretch

EXPOSE 8080

COPY --from=builder /home/gradle/src/build/libs/u2f-nfc-api-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app

COPY ["vendor/libacsccid1_1.1.5-1~bpo9+1_amd64.deb", "/app/"]
RUN apt-get update && \
    apt-get install libccid pcscd libpcsclite-dev libpcsclite1 -y && \
    dpkg -i libacsccid1_1.1.5-1~bpo9+1_amd64.deb

CMD ["java", "-jar", "u2f-nfc-api-0.0.1-SNAPSHOT.jar"]
