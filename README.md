# U2F Rest proxy to PC/SC NFC devices

### Prerequisite
You need to start PC/SC service on host machine with appropriate drivers.
For example start PCSC service in Ubuntu/Debian with ACR122U device.
```sh
apt-get install libccid pcscd libpcsclite-dev libpcsclite1

wget https://www.acs.com.hk/download-driver-unified/9232/ACS-Unified-PKG-Lnx-115-P.zip
# extract for debian and:
dpkg -i libacsccid1_1.1.5-1~bpo9+1_amd64.deb

# Start service
systemctl start pcscd.service
```

### Build and start
Build:
```sh
docker build -t quckly/u2f-nfc-api:latest .
``` 

Run:
```sh
docker run --rm -ti -p 127.0.0.1:8080:8080 -v /var/run/pcscd/pcscd.comm:/var/run/pcscd/pcscd.comm quckly/u2f-nfc-api:latest
``` 

### Usage
#### Register request
Send payload
```json
{"challenge": "WocoHNB2dFCZCvQxwxEzzZFNZ0cP4yDH0x4kcTuUl18", "version": "U2F_V2", "appId": "http://demo.yubico.com", "origin":"https://demo.yubico.com"}
```
```sh
curl -X POST -H 'Content-Type: application/json' -i 'http://127.0.0.1:8080/register' --data '{"challenge": "WocoHNB2dFCZCvQxwxEzzZFNZ0cP4yDH0x4kcTuUl18", "version": "U2F_V2", "appId": "http://demo.yubico.com", "origin":"https://demo.yubico.com"}'
```

#### Authenticate request
Send payload
```json
{"challenge": "KIlZ6fT1RfE-ZT8WijOQx1tJ3R1E2UjO6cWZhWoPY6o", "keyHandle": "FuBqr8YfvjbH2emIZkk3DQ", "appId": "http://demo.yubico.com", "origin": "https://demo.yubico.com"}
```
```sh
curl -X POST -H 'Content-Type: application/json' -i 'http://127.0.0.1:8080/authenticate' --data '{"challenge": "KIlZ6fT1RfE-ZT8WijOQx1tJ3R1E2UjO6cWZhWoPY6o", "keyHandle": "FuBqr8YfvjbH2emIZkk3DQ", "appId": "http://demo.yubico.com", "origin": "https://demo.yubico.com"}'
```